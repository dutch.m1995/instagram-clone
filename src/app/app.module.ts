import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InstagramHeaderComponent } from './instagram-header/instagram-header.component';
import { TimelineComponent } from './timeline/timeline.component';
import { MessageComponent } from './message/message.component';
import { LoginComponent } from './login/login.component';
import { Friend1Component } from './friend1/friend1.component';

@NgModule({
  declarations: [
    AppComponent,
    InstagramHeaderComponent,
    TimelineComponent,
    MessageComponent,
    LoginComponent,
    Friend1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
