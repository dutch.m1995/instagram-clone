import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimelineComponent } from './timeline/timeline.component';
import { MessageComponent } from './message/message.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path:"",component:TimelineComponent
  },
  {
    path:"message",component:MessageComponent
  },
  {
    path:"login",component:LoginComponent
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
